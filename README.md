# API ��� ����������� �����������


## ��������� ���� �����������

### GET /api/v1/pictures
http://test2.corsawos.ru/api/v1/pictures

���� ����� ���������� ������ ���� ����������� ����������� � ������� JSON.

### ������ ������

`{
    "status_code": 200,
    "message": "success",
    "data": [
        {
            "id": 1,
            "file_name": "domvkotorom_1710340047.jpg",
            "created_at": "2024-03-13T14:27:27.000000Z",
            "updated_at": "2024-03-13T14:27:27.000000Z"
        },
        {
            "id": 2,
            "file_name": "27ccda68174c_1710340152.jpg",
            "created_at": "2024-03-13T14:29:12.000000Z",
            "updated_at": "2024-03-13T14:29:12.000000Z"
        },
        {
            "id": 3,
            "file_name": "269_1710340152.jpg",
            "created_at": "2024-03-13T14:29:12.000000Z",
            "updated_at": "2024-03-13T14:29:12.000000Z"
        },
	...
    ]
}`

## ��������� ���������� � ����������� ����������� �� id

### GET /api/v1/picture/{id}
http://test2.corsawos.ru/api/v1/pictures/5

### ������ ������

`{
    "status_code": 200,
    "message": "success",
    "data": {
        "id": 5,
        "file_name": "ava-wint10_1710351937.jpg",
        "created_at": "2024-03-13T17:45:37.000000Z",
        "updated_at": "2024-03-13T17:45:37.000000Z"
    }
}`

���� ����������� � ��������� id �� �������, ����� ��������� ������ ������ 404 � ���������� �� ������.

{
    "error": "Picture not found",
    "status_code": 404
}
