<!DOCTYPE html>
<html>
<head>
    <title>Uploaded Images</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

<div class="container mt-4">
    <h2>Upload Pictures</h2>
    @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="/upload" method="post" enctype="multipart/form-data">
        @csrf
        <input type="file" name="pictures[]" multiple>
        <button type="submit">Upload</button>
    </form>
    <br><span  id="sort-name" class="btn btn-primary">sort by name</span> <span  id="sort-name2" class="btn btn-primary">sort by name from the end</span> <span  id="sort-date" class="btn btn-primary">oldest</span>  <span  id="sort-date2" class="btn btn-primary">newest</span><br><br>
    <div id="images-container" class="row">
        @foreach ($pictures as $picture)
            <div class="col-md-4 mb-3">
                <div class="card">
                    <img src="{{ asset('storage') . '/pictures/'. $picture->file_name }}" class="card-img-top" alt="Thumb">
                    <div class="card-body">
                        <h5 class="card-title" id="{{ $picture->id }}">{{ $picture->file_name }}</h5>
                        <span class="card-date">{{$picture->created_at->format('d.m.Y H:i:s')}}</span><br /><br />
                        <a href="{{ asset('storage') . '/pictures/'. $picture->file_name }}" target="_blank" class="btn btn-primary">Original</a><br />
                        <button class="btn btn-primary mt-3 download-zip" rel="{{ $picture->id }}">Download as ZIP</button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        // Сортировка по имени
        $('#sort-name').click(function() {
            var sortedImages = $('#images-container .col-md-4').sort(function(a, b) {
                var aName = $(a).find('.card-title').text().toLowerCase();
                var bName = $(b).find('.card-title').text().toLowerCase();
                return aName.localeCompare(bName);
            });
            $('#images-container').html(sortedImages);
        });

        // Сортировка по имени с конца
        $('#sort-name2').click(function() {
            var sortedImages = $('#images-container .col-md-4').sort(function(a, b) {
                var aName = $(a).find('.card-title').text().toLowerCase();
                var bName = $(b).find('.card-title').text().toLowerCase();
                return bName.localeCompare(aName);
            });
            $('#images-container').html(sortedImages);
        });

        // Сортировка по дате
        $('#sort-date').click(function() {
            var sortedImages = $('#images-container .col-md-4').sort(function(a, b) {
                var aName = $(a).find('.card-title').attr('id').toLowerCase();
                var bName = $(b).find('.card-title').attr('id').toLowerCase();
                return aName - bName;
            });
            $('#images-container').html(sortedImages);
        });

        // // Сортировка по дате с конца
        $('#sort-date2').click(function() {
            var sortedImages = $('#images-container .col-md-4').sort(function(a, b) {
                var aName = $(a).find('.card-title').attr('id').toLowerCase();
                var bName = $(b).find('.card-title').attr('id').toLowerCase();
                return bName - aName;
            });
            $('#images-container').html(sortedImages);
        });

        // Скачать картинку в zip
        $('#images-container').on('click', '.download-zip', function(){
        //$('.download-zip').click(function() {
            var id = $(this).attr("rel");
            window.location.href = '/download-zip/' + id;
        });
    });
</script>
</body>
</html>
