<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PictureController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PictureController::class, 'index'])->name('pictures.index')->middleware('auth.basic');
Route::post('/upload', [PictureController::class, 'upload'])->middleware('auth.basic');
Route::get('/download-zip/{picture_id}', [PictureController::class, 'downloadZip'])->middleware('auth.basic');
