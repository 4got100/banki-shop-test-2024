<?php

namespace App\Http\Controllers;

use App\Models\Picture;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use ZipArchive;
use Redirect;
use Illuminate\Support\Facades\Auth;

class PictureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pictures = Picture::orderBy('id')->get();
        return view('upload', compact('pictures'));
    }

    // Загрузка файлов
    public function upload(Request $request)
    {

        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $request->validate([
            'pictures.*' => [
                'image',
                'mimes:jpeg,png,jpg,gif',
                'max:2048'
            ],
            'pictures' => 'max:5',
        ], [
            'pictures.max' => 'You can upload a maximum of 5 images at once.'
        ]);
        $uploadedPictures = [];

        foreach ($request->file('pictures') as $picture) {
            $fileName = Str::slug(pathinfo($picture->getClientOriginalName(), PATHINFO_FILENAME)) . '_' . time() . '.' . $picture->getClientOriginalExtension();
            $picture->storeAs('public/pictures', $fileName);
            $newPicture = new Picture();
            $newPicture->file_name = $fileName;
            $newPicture->save();
            $uploadedPicture[] = $fileName;
        }
        return Redirect::to('/');

    }

    // Упаковка картинки в zip и скачивание
    public function downloadZip($picture_id)
    {
        $picture = Picture::where('id', $picture_id)->first();
        $zipFileName = 'picture_' . time() . '.zip';
        $zip = new ZipArchive;
        if ($zip->open(public_path($zipFileName), ZipArchive::CREATE) === TRUE){
            $filePath = public_path('storage/pictures/' . $picture->file_name);
            $zip->addFile($filePath, $picture->file_name);
        }
        $zip->close();
        return response()->download(public_path($zipFileName))->deleteFileAfterSend(true);
    }

    // Вывод всех картинок
    public function getAllPictures()
    {
        try {
            $pictures = Picture::all();
            $response = $this->prepareSuccessResponse($pictures);
            return response()->json($response, $response['status_code']);
        }
        catch (Exception $e) {
            return response()->json(['error' => $e->getMessage(), 'status_code' => 500], 404);
        }
    }

    // Получить картинку по id
    public function getPictureById($id)
    {
        try {
            $picture = Picture::where('id', $id)->first();
            if (!$picture) {
                return response()->json(['error' => 'Picture not found', 'status_code' => 404], 404);
            }

            $response = $this->prepareSuccessResponse($picture);
            return response()->json($response, $response['status_code']);
        }
        catch (Exception $e) {
                return response()->json(['error' => $e->getMessage(), 'status_code' => 500], 404);
            }
    }

    // Подготовка успешного ответа API
    private function prepareSuccessResponse($data) {
        $statusCode = 200;
        $message = 'success';
        $arr = [
            'status_code' => (isset($statusCode)) ? $statusCode : 500,
            'message' => (isset($message)) ? $message : 'error'
        ];
        $arr['data'] = $data;
        return $arr;
    }
}
